// Q1:
// You have to design a programme/ function to achieve below objectives:
// Given that there is an array contains number from 1 to 100.
// When the number is multiple of 3, print &quot;bug&quot;
// When the number is multiple of 5, print &quot;fix&quot;
// When the number is multiple of 3 and 5, print &quot;bugfix&quot;
export function logMultiples(arr: number[]) {
  let m3 = new Set();
  let m5 = new Set();
  let m3And5 = new Set();
  for (let i = 0; i < arr.length; i++) {
    if (arr[i] >= 3) {
      if (arr[i] % 3 == 0 && arr[i] % 5 == 0 && arr[i] >= 5) {
        m3.add(arr[i]);
        m5.add(arr[i]);
        m3And5.add(arr[i]);
        console.log("bugfix");
      }
      if (arr[i] % 3 == 0) {
        m3.add(arr[i]);
        console.log("bug");
      }
      if (arr[i] % 5 == 0 && arr[i] >= 5) {
        m5.add(arr[i]);
        console.log("fix");
      }
    }
  }

  console.log(`multiple of 3: ${Array.from(m3)}`);
  console.log(`multiple of 5: ${Array.from(m5)}`);
  console.log(`multiple of 3 and 5: ${Array.from(m3And5)}`);
}

logMultiples(Array.from({ length: 100 }, (_, x) => x + 1));

import * as funcs from "../q2-q5";

test("should return difference of two arrays", () => {
  expect(funcs.difference([1, 2, 3, 4, 5], [2, 3, 1, 0, 5])).toEqual([4]);
});

test("should return intersection of two arrays in ascending order", () => {
  expect(funcs.intersection([1, 2, 3, 4, 5], [2, 3, 1, 0, 5])).toEqual([
    1, 2, 3, 5,
  ]);
});

test("should return union of two arrays in ascending order", () => {
  expect(funcs.union([1, 2, 3, 4, 5], [2, 3, 1, 0, 5])).toEqual([
    0, 1, 2, 3, 4, 5,
  ]);
});

test("should return closest to 17 in the given array", () => {
  expect(funcs.findClosestTo17([30, 1, 5, 16, 19, 21, 2, 55])).toEqual(16);
});

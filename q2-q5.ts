// Q2:
// You have to design a programme/ function to achieve below objectives:
// Given two arrays, [1,2,3,4,5] and [2,3,1,0,5]
// find which number(s) is/are not present in the second array
// Answer: [4]
export function difference(arr1: number[], arr2: number[]) {
  return arr1.filter((value) => !arr2.includes(value)).sort();
}

//Q3
// You have to design a programme/ function to achieve below objectives:
// Given two arrays, [1,2,3,4,5] and [2,3,1,0,5]
// find which number(s) is/are common in both array
// Answer: [1, 2, 3, 5]
export function intersection(arr1: number[], arr2: number[]) {
  return arr1.filter((value) => arr2.includes(value)).sort();
}

//Q4
// You have to design a programme/ function to achieve below objectives:
// Given two arrays, [1,2,3,4,5] and [2,3,1,0,5]
// merge these two arrays and unique to display
// Answer: [0 ,1, 2, 3, 4, 5]
export function union(arr1: number[], arr2: number[]) {
  const arr = new Set([...arr1, ...arr2]);
  return Array.from(arr).sort();
}

//Q5
// You have to design a programme/ function to achieve below objectives:
// How do you find the closest integer 17 in [30, 1, 5, 16, 19, 21, 2, 55]?
// Answer: 16
export function findClosestTo17(arr: number[]) {
  let closest = arr[0];
  let closestDiff = Math.abs(closest - 17);
  for (let i = 0; i < arr.length; i++) {
    let currentDiff = Math.abs(arr[i] - 17);
    if (currentDiff < closestDiff) {
      closest = arr[i];
      closestDiff = Math.abs(arr[i] - 17);
    }
  }
  return closest;
}

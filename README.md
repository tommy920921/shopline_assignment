# Shopline technical questions

## HOW TO TEST(Q1-Q5)

Install required node_modules by

```bash
yarn install
```

Then run jest test cases(for Q2-Q5) by

```bash
yarn test
```

print results for Q1

```bash
npx ts-node q1.ts
```

## Other Questions and Answers

1. Explain what RESTful API is

    `A REST API (also known as RESTful API) is an application programming interface (API or web API) that conforms to the constraints of REST architectural style and allows for interaction with RESTful web services. REST stands for representational state transfer and was created by computer scientist Roy Fielding.`

2. How do you form an api endpoint to query with below resources
    - a. Page: 10
    - b. Book: odyssey
    - c. Library: GreatLibrary

    `GET https://api.example.com/v1/libraries/GreatLibrary/books/odyssey?page=10`

3. Explain what MVC is

- Model–view–controller (MVC) is a software design pattern commonly used for developing user interfaces that divide the related program logic into three interconnected elements. This is done to separate internal representations of information from the ways information is presented to and accepted from the user.`

- Model
  - The central component of the pattern. It is the application's dynamic data structure, independent of the user interface.[10] It directly manages the data, logic and rules of the application.
- View
  - Any representation of information such as a chart, diagram or table. Multiple views of the same information are possible, such as a bar chart for management and a tabular view for accountants.`
- Controller
  - Accepts input and converts it to commands for the model or view.
   In addition to dividing the application into these components, the model–view–controller design defines the interactions between them.
- The model is responsible for managing the data of the application. It receives user input from the controller.`
- The view renders presentation of the model in a particular format.
 The controller responds to the user input and performs interactions on the data model objects. The controller receives the input, optionally validates it and then passes the input to the model.

1. Explain what OOP is

- Object-oriented programming (OOP) is a style of programming characterized by the identification of classes of objects closely linked with the methods (functions) with which they are associated. It also includes ideas of inheritance of attributes and methods.
